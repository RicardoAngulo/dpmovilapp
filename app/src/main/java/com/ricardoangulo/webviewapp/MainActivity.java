package com.ricardoangulo.webviewapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import androidx.appcompat.app.AppCompatActivity;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

public class MainActivity extends AppCompatActivity {
    WebView _webView;
    SwipeRefreshLayout _swipeRefreshLayout;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        _webView = findViewById(R.id.activity_main_web_view);
        _swipeRefreshLayout = findViewById(R.id.swipe_refresh);
        _webView.loadUrl(Constants.Uri);
        _webView.getSettings().setJavaScriptEnabled(true);
        _webView.setWebViewClient(new WebViewClient());

        _swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                _swipeRefreshLayout.setRefreshing(true);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        _swipeRefreshLayout.setRefreshing(false);
                        _webView.loadUrl(Constants.Uri);
                    }
                },  3000);
            }
        });

        _swipeRefreshLayout.setColorSchemeColors(
                getResources().getColor(android.R.color.holo_blue_bright),
                getResources().getColor(android.R.color.holo_orange_dark),
                getResources().getColor(android.R.color.holo_green_dark),
                getResources().getColor(android.R.color.holo_red_dark));
    }
}