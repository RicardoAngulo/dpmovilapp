# DP Movil App

Práctica en android que embebe una página web mediante webview

NOTA: Para esta versión del proyecto se utilizó la versión 4.1 de Android Studio.

##     ⚡️Pre requisitos ##
1. Contar con la ultima version de [Java](https://www.java.com/es/download/) y el entorno de desarrollo [Android Studio](https://developer.android.com/studio/index.html?hl=es-419)
2. Contar con un SmartPhone Android y cable de datos, el celular debe de contar con la opción de depuración USB habilitada.

##     Para comenzar ##
1. Clonar este repositorio desde el CMD o git bash posicionandote en cualquier parte del equipo:
git clone https://gitlab.com/RicardoAngulo/dpmovilapp.git
2. Iniciar el proyecto
3. Iniciar la depuración o ejecución del proyecto.

## Correr la aplicación

Para correr la aplicación solo necesitas instalar el APK y contar con acceso a internet.
```
app-debug.apk
```

## Dudas y comentarios les dejo mi correo:

ricardoangulo_@hotmail.com
